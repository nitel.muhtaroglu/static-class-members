package cs102;

public class Student {
    private String name;
    private int id;
    static int studentCount = 0;

    public Student(String name) {
        this.name = name;
        this.id = ++Student.studentCount;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public static int getStudentCount() {
        return Student.studentCount;
    }

    public static String[] days = new String[7];

    static {
        days[0] = "Monday";
        days[1] = "Tuesday";
        days[2] = "Wednesday";
        days[3] = "Thursday";
        days[4] = "Friday";
        days[5] = "Saturday";
        days[6] = "Sunday";
    }
}
